FROM atlas/analysisbase:21.2.75

COPY source /Bootcamp/source 

WORKDIR /Bootcamp/build

RUN sudo mkdir /Bootcamp/run

RUN source ~/release_setup.sh && \
    sudo chown -R atlas /Bootcamp  && \
    cmake ../source && \
    make